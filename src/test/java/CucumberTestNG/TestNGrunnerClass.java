package CucumberTestNG;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
features = "src/test/resources/features/TestNG.feature",
glue={"Com.TestNG"})

public class TestNGrunnerClass extends AbstractTestNGCucumberTests {

}
